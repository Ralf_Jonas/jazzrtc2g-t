#!/usr/bin/perl
#
# Script to convert an entire Jazz stream with several (or just one) component into separate git repositories.
# The script has been developed and started on Windows 7.0.
# It has been successfully tested with Jazz server version 5.0 but should work with older versions also.
# Written 2015 by Ralf Jonas and given to the public domain. Any comments to ralf.jonas@gmx.de
#
use strict;
use warnings;
use Data::Dumper;
use File::Path;
use IPC::Open3;
use constant MY_USER_DIR => "c:\\Users\\XYZ";                     # Your windows user directory
use constant ACTIVE_FLAG => MY_USER_DIR . "\\.migration.active";
use constant COMPONENTS_UUID_FILE => MY_USER_DIR . "\\.migration.components_uuid";

my $SCM_EXE = '"C:\Program Files\jazz\scmtools\eclipse\fec.exe"'; # Path to your scm executable.
my $JAZZ_REPOS = "https://jazz.xxyyzz.net/ccm/";                  # URL to your JAZZ repositories.
my $JAZZ_USER = "UserName";                                       # Your JAZZ username.
my $JAZZ_PW = '"Password"';                                       # Your JAZZ password.
my $JAZZ_STREAM = '"Development_Stream"';                         # Name of the stream to convert.
my $JAZZ_WORKSPACE =  'Jazz2Git_MigrationWorkspace';              # Name of a temporary workspace 
                                                                  # created automagically by this script.
																  
my $JAZZ_MAXNUM_CHANGESETS = 15000;                               # Choose these numbers wisely. 
my $JAZZ_MAXNUM_BASELINES  = 10000;                               # too big -> oom exception

my $LOCAL_FILESTORE = "C:\\1";                                    # Here are your local files

my $GIT_MASTER_DIRECTORY = "C:\\2";                               # Your git repos are created here.

my $SIMULATION = 0;                                               # Set to 1 to test accept all changes.
                                                                  # Sometimes not all changes are 
                                                                  # applyable due to conflicts, so I 
																  # omitted them.
																  
my $DEFAULT_EMAIL = '<developer@company.com>';                    # Dummy Address for all authors.																  

# We had some issues with UTF-16 chars.	
sub translateUtf16 {
	my $str = shift;
	$str =~ s/\\u00f6/�/g;
	$str =~ s/\\u00e4/�/g;
	$str =~ s/\\u00fc/�/g;
	$str =~ s/\\u00c4/�/g;
	$str =~ s/\\u00d6/�/g;
	$str =~ s/\\u00dc/�/g;
	return $str
}

sub setInProgressFlag {
	open my $fhflag, ">" . ACTIVE_FLAG || die "Unable to create active flag " . ACTIVE_FLAG . ": $!";
	print $fhflag "Active";
	close $fhflag;
}

sub resetInProgressFlag {
	unlink ACTIVE_FLAG;
}

sub checkInProgressFlag {
	return -f ACTIVE_FLAG;
}

sub loginJazz {
	system ("$SCM_EXE login -r $JAZZ_REPOS -u $JAZZ_USER -P $JAZZ_PW") == 0 || die "Unable to login.";
}

sub logoutJazz {
	system ("$SCM_EXE logout -r $JAZZ_REPOS");
}

# Login to Jazz
print "Login to JAZZ\n";
loginJazz;

my @components = ();

if (!checkInProgressFlag) {	
	# Delete old Workspace
	print "Delete workspace in JAZZ\n";
	system ("$SCM_EXE delete workspace \"$JAZZ_WORKSPACE\" -r $JAZZ_REPOS ");

	# Create new Workspace
	print "Create new workspace in JAZZ\n";
	system ("$SCM_EXE create workspace \"$JAZZ_WORKSPACE\" -r $JAZZ_REPOS -s $JAZZ_STREAM");

	# Get UUID of Stream
	print "Get UUID for stream\n";
	open my $fh, '-|', "$SCM_EXE --show-alias n --show-uuid y show attributes -r $JAZZ_REPOS -w $JAZZ_STREAM" || die "Unable to read stream uuid";
	(readline $fh) =~ /\((.*)\)/;
	close $fh;
	my $streamUUID = $1;
	print "UUID of stream $JAZZ_STREAM is $streamUUID\n";
	
	# Get Components in stream
	print "Get components in stream\n";
	open $fh, '-|', "$SCM_EXE --show-alias n --show-uuid y list components -v -r $JAZZ_REPOS $streamUUID" || die "Unable to read stream uuid";
	while (<$fh>) {
		chomp;
		if (/Komponente\: \((.*)\) \"(.*)\"/) {
			print "Component $2, UUID $1\n";
			push @components, {name => $2, uuid => $1, changesets => []};
		}
	} 
	close $fh;

	open my $fh2, ">" . COMPONENTS_UUID_FILE || die "Unable to create components uuid file " . COMPONENTS_UUID_FILE . ": $!";
	for my $c (@components) {
	    print $fh2 "$c->{name}=$c->{uuid}\n";
	}
	close $fh2;

	if (-d $GIT_MASTER_DIRECTORY) {
		print "Removing git master directory $GIT_MASTER_DIRECTORY.\n";
		# rmtree $GIT_MASTER_DIRECTORY;
	}	

	print "Creating git master directory $GIT_MASTER_DIRECTORY.\n";
	mkdir $GIT_MASTER_DIRECTORY;

	setInProgressFlag;
} else {
	open my $fh, "<" . COMPONENTS_UUID_FILE or die "Unable to open components uuid file " . COMPONENTS_UUID_FILE . ": $!";
	while (<$fh>) {
		if (/(.*)=(.*)/) {
			push @components, {name => $1, uuid => $2, changesets => []};
			print "Read component from file. Name: $1, UUID: $2\n";
		}
	}
	close $fh;
}

for my $component (@components) {
	next if $component->{name} eq 'xxyxyyyzzz';  # If you want to skip some components of the stream...
	
    chdir "c:\\";
	
    if (-d $LOCAL_FILESTORE) {
		print "Removing working directory $LOCAL_FILESTORE.\n";
		rmtree $LOCAL_FILESTORE;
	}	

	print "Creating working directory $LOCAL_FILESTORE.\n";
	mkdir $LOCAL_FILESTORE;
	
	print "Changing active directory to $LOCAL_FILESTORE.\n";
	chdir $LOCAL_FILESTORE;

	my $gitRepos = "$GIT_MASTER_DIRECTORY\\$component->{name}\\.git";	
	my $changesetFilename = MY_USER_DIR . "\\.migration_changeset_" . $component->{uuid};
	my @changeset_uuids = ();
	
	if (-e $changesetFilename) {
		open (my $fh, "<", $changesetFilename) or die "Unable to open changesetfile $changesetFilename: $!";
		while (<$fh>) {
			chomp;
			push @changeset_uuids, $_ if length $_ > 10;
		}
		close $fh;
	} else {
		print "Loading changeset overview for component $component->{name}\n";
		
		#
		# Sometimes "list changesets" results in "server error", "oom exceptions" and other weird things.
		# In this case set the environment variable JAVA_OPTS="-Xmx2048M" before starting this script.
		#
		
		open my $fh, '-|', "$SCM_EXE list changesets -r $JAZZ_REPOS -w $JAZZ_WORKSPACE -C $component->{uuid} -m $JAZZ_MAXNUM_CHANGESETS -j";
		while (<$fh>) {
			if (/"uuid": "(.*)"/) {
				unshift @changeset_uuids, $1
			}		
		}
		close $fh;
		die "Unable to read changesets from server" if @changeset_uuids == 0;
		
		open ($fh, ">", $changesetFilename) or die "Unable to create changesetfile $changesetFilename: $!";
		for my $xy (@changeset_uuids) {
			print $fh "$xy\n";
		}
		close $fh;
	}

	my $lastChangesetFilename = MY_USER_DIR . "\\.migration_last_changeset_" . $component->{uuid};
	my $lastChangeset = "";
	if (-e $lastChangesetFilename) {
		open (my $fh, "<", $lastChangesetFilename) or die "Unable to open changesetfile $lastChangesetFilename: $!";
		while (<$fh>) {
			chomp;
			$lastChangeset = $_ if length $_ > 10;
		}
		close $fh;	    
	} else {
		print "Loading baselines of component.\n";
		my $baseline_uuid = "";
		my $smallestID = 1000000;
		my $currentID = -1;
		open my $fh, '-|', "$SCM_EXE list baselines -r $JAZZ_REPOS -j -m $JAZZ_MAXNUM_BASELINES --components $component->{uuid}";
		while (<$fh>) {
			if (/"id": (\d+)/) {
				$currentID = 0 + $1;
				if ($currentID > $smallestID) {
					$smallestID = -1;
				} else {
					$smallestID = $currentID;
				}
			}
			if ($currentID >= 0 && /"uuid": "(.*)"/) {
				$currentID = -1;
				$baseline_uuid = $1;
			}
		}
		close $fh;
		
		print "Setting component to earliest baseline $baseline_uuid\n";
		system "$SCM_EXE set component -r $JAZZ_REPOS -b $baseline_uuid $JAZZ_WORKSPACE workspace $JAZZ_WORKSPACE $component->{uuid}";

		print "Creating git repository $GIT_MASTER_DIRECTORY\\$component->{name}\n";
		system ("git",  "init", "$GIT_MASTER_DIRECTORY\\$component->{name}");
		
		print "Loading component into working directory $LOCAL_FILESTORE.\n";	
		system ("$SCM_EXE load -r $JAZZ_REPOS --force $JAZZ_WORKSPACE $component->{uuid}");		

		open $fh, ">.gitignore" || die "Unable to create file .gitignore";
		print $fh ".jazz5/\r\n"; # Adjust this to your version of Jazz, except you want to migrate these files as well.
		close $fh;
	}
	
	print "Reading changesets of component $component->{name}\n";
	my $counter = 0;
	my $firstAccept = 1;
	for my $uuid (@changeset_uuids) {
		if ($lastChangeset ne "") {
			next if $lastChangeset ne $uuid;
			$lastChangeset = "";
			next;
		}
	
		# Ommitting "bad" changesets (e.g. bad file or directory names, unresolved conflicts, ...)
		if (grep {$_ eq $uuid} ("_cf9bQUS0EeS4fd38lnw30Q", "_rrkCwUS0EeS4fd38lnw30Q")){
			print "Omitting changeset $uuid\n";
			next;
		}
		
		my %changeSet = ();
		unless ($SIMULATION) {		
			print "Reading details of changeset $uuid\n";
			open my $fh, '-|', "$SCM_EXE list changes -r $JAZZ_REPOS -w $JAZZ_WORKSPACE -j $uuid";
			my $lastPart = 0;
			while (<$fh>) {
				if (/"author": "(.*)"/) {
					$changeSet{author} = translateUtf16 $1; 
					# print "Autor : $1\n";
				}
				if (/"comment": "(.*)"/) {
					$changeSet{comment} = translateUtf16 $1;
					# print "Kommentar : $1\n";
				}
				if (/"modified": "(.*)"/) {
					$changeSet{date_modified} = translateUtf16 $1;
					# print "�nderungsdatum : $1\n";
					$lastPart = 1;
				}
				if ($lastPart && /"uuid": "(.*)"/) {
					$changeSet{uuid} = $1;
					# print "UUID : $1\n";
					unshift @{$component->{changesets}}, \{%changeSet};
					$lastPart = 0;
					# %changeSet = ();
				}
			}
			close $fh;
		}
		
		print "Accepting changeset $changeSet{comment}\n" unless $SIMULATION;
		
		if (system ("$SCM_EXE accept -r $JAZZ_REPOS -t $JAZZ_WORKSPACE --overwrite-uncommitted --accept-missing-changesets -N -c $uuid")) {
			# Get the error message:
			my ($fhin, $fhout, $pid);
			$pid = open3($fhin, $fhout, undef, $SCM_EXE, ("accept", "-r", $JAZZ_REPOS, "-t", $JAZZ_WORKSPACE, "--overwrite-uncommitted", "--accept-missing-changesets", "-N", "-c", $uuid));
			my $cont = 0;
			while (<$fhout>) {
				if (/CRRTC5094E/) {
					$cont = 1;
					last;
				}
				if (/ist bereits im Verlaufsprotokoll enthalten/) {
					$cont = 1;
					last;
				}
			}
			waitpid($pid, 0);
			
			unless ($cont) {
				print "Error accepting a changeset while executing the following command:\n";
				print "$SCM_EXE accept -v -r $JAZZ_REPOS -t $JAZZ_WORKSPACE --overwrite-uncommitted --accept-missing-changesets -N -c $uuid\n";
				# Give it a second try...
				if (system ("$SCM_EXE accept -v -r $JAZZ_REPOS -t $JAZZ_WORKSPACE --overwrite-uncommitted --accept-missing-changesets -N -c $uuid")) {
					# Didn't work, so...
					print "Unable to call this command successfully:\n";
					print "$SCM_EXE accept -v -r $JAZZ_REPOS -t $JAZZ_WORKSPACE --overwrite-uncommitted --accept-missing-changesets -N -c $uuid\n";
					print "Please repair by hand, then terminate the shell with 'exit'\n";
					# This command starts a user shell and waits for "exit".
					system("cmd");
				}				
			}
		} 
		
		unless ($SIMULATION) {
			print "Loading fileset into local workspace.\n";
			system ("$SCM_EXE load -r $JAZZ_REPOS --force $JAZZ_WORKSPACE $component->{uuid}");	
			
			print "Adding changes into Git.\n";
			system ("git", "--git-dir=$gitRepos", "add", "-A");
			
			open my $fh, ">c:/message.txt";
			print $fh $changeSet{comment};
			close $fh;
			
			my $jazzDate = $changeSet{date_modified};
			$jazzDate =~  /(\d+)-(.*?)-(\d+)\ (\d+):(\d+)\ (\w+)/;
			my $gitDate = sprintf "%02d.%02d.%04d %02d:%02d", (0+$1), translateMonth($2), (0+$3), amPmTo24h ($4, $6), (0+$5);
			
			print "Jazz date: $jazzDate, Git date: $gitDate\n";
			
			print "Committing into Git with author $changeSet{author} and date $changeSet{date_modified}\n";
			system ("git", "--git-dir=$gitRepos", "commit", "-F", "c:\\message.txt", 
					"--author", $changeSet{author} . " $DEFAULT_EMAIL", "--date", $gitDate);		
			unlink "c:\\message.txt";					
		}
		print '' . (localtime) . "\n";
		$firstAccept = 0;
		
		open (my $fh1, ">", $lastChangesetFilename) or die "Unable to open changesetfile $lastChangesetFilename: $!";
		print $fh1 "$uuid\n"; 
		close $fh1;	    
	}
}

# Delete Workspace
print "Delete workspace in JAZZ\n";
system ("$SCM_EXE delete workspace \"$JAZZ_WORKSPACE\" -r $JAZZ_REPOS ");

print "Logging out\n";
logoutJazz;
resetInProgressFlag;

sub amPmTo24h {
	my ($hour, $ampm) = @_;
	my $value = 0+$hour;
	# 12 PM is midnight, 12 AM is noon.
	if ($hour eq '12')  {
		$value = 0;
		$value = 12 if $ampm eq 'PM';
	} else {
		$value += 12 if $ampm eq 'PM';
	}
	return $value;
}

sub translateMonth {
	my $month = shift;
	return 1 if $month eq 'Jan';
	return 2 if $month eq 'Feb';
	return 3 if $month eq 'M�r';
	return 4 if $month eq 'Apr';
	return 5 if $month eq 'Mai';
	return 6 if $month eq 'Jun';
	return 7 if $month eq 'Jul';
	return 8 if $month eq 'Aug';
	return 9 if $month eq 'Sep';
	return 10 if $month eq 'Okt';
	return 11 if $month eq 'Nov';
	return 12 if $month eq 'Dez';
	die "Error converting $month";
}


